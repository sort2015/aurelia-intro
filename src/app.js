export class App {
  configureRouter(config, router) {
    config.title = 'Aurelia';
    config.map([
      {route: ['', 'home'], name: 'home', moduleId: 'home', nav: true, title: 'Home'},
      //{route: 'tasks', name: 'tasks', moduleId: 'tasks', nav: true, title: 'Tasks'},
    ]);

    this.router = router;
  }
}
